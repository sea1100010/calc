#!/bin/bash

if [ $# -eq 2 ]
 then
  file1=$(cat ${1})
  file2=$(cat ${2})
 else
  echo "Must be 2 arguments" 
fi

if [ $((file1)) -ge $((file2)) ] 
 then echo $((file1))
 else echo $((file2))
fi
 
